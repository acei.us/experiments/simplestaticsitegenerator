<?php
/************************************
 * SimpleStaticSiteGenerator
 * Copyright © 2022 - 2023 AceiusIO
 * Licenced under MIT
 ***********************************/

require_once __DIR__ . "/vendor/autoload.php";

use Symfony\Component\Console\Application;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\SingleCommandApplication;

$version = "0.6.0-dev";
$application = new Application();

(new SingleCommandApplication())
    ->setName('SimpleStaticSiteGenerator')
    ->setVersion('0.6.0')
    ->addArgument('foo', InputArgument::OPTIONAL, 'The directory')
    ->addOption('bar', null, InputOption::VALUE_REQUIRED)
    ->setCode(function (InputInterface $input, OutputInterface $output) {
        $parsedown = new Parsedown();
        if (mb_substr($input, -1) == "/") {
            $input_path = rtrim($input, "/");
            //noslash!
        }
        $output_path = $input_path . "/dist/";
        $config = (array) json_decode(file_get_contents($input_path . "/config.json"));
        $files = array_diff(
            scandir($input_path),
            array('.', '..', "config.json", "dist")
        );

        if (!is_dir($output_path)) {
            mkdir($output_path);
        }

        print_r("Creating userstyle.css... ");
        file_put_contents(
            $output_path . "userstyle.css",
            'main{background-color:' . $config["secondary-color"] . ' !important;color:' . $config["secondary-text"] . ' !important;} body{background-color:' . $config["primary-color"] . ' !important;color:' . $config["primary-text"] . ' !important;}'
        );
        print_r("Done.\n");

        print_r("Generating component:navbar... ");
        $component_navbar = '<nav id="navigation" class="container bar"><a href="/" class="bar-item button">' . $config["title"] . '</a>';
        foreach ($files as $page) {
            $component_navbar = $component_navbar . '<a href="/' . basename($input_path . "/" . $page, ".md") . '.html" class="bar-item button">' . ucfirst(basename($input_path . "/" . $page, ".md")) . '</a>';
        }
        $component_navbar = $component_navbar . '</nav>';
        print_r("Done.\n");

        foreach ($files as $page) {
            print_r("Generating " . basename($input_path . "/" . $page, ".md") . ".html... ");
            $output = '<!DOCTYPE html>
<html>
    <head>
        <title>' . ucfirst(basename($input_path . "/" . $page, ".md")) . ' - ' . $config["title"] . '</title>        
        <link href="https://acei.us/assets/css/w3.css" rel="stylesheet" type="text/css" />
        <link href="userstyle.css" rel="stylesheet" type="text/css" />
        <meta charset="utf-8">
    </head>
    <body>
        <header id="top">
            ' . $component_navbar . '
        </header>
        <main>
            <div class="container">
                ' . $parsedown->text(file_get_contents($input_path . "/" . $page)) . '
            </div>
        </main>
        <footer>
            <div>
                <div class="bar container">
                    <a href="#top" class="bar-item button">Back to Top</a>
                </div>
            </div>
        </footer>
    </body>
</html>';
            print_r("Done.\nWriting to " . basename($input_path . "/" . $page, ".md") . ".html... ");
            file_put_contents(
                $output_path . basename($input_path . "/" . $page, ".md") . ".html",
                $output
            );
            print_r("Done.\n");
        }
    })
    ->run();
