<?php
/************************************
 * SimpleStaticSiteGenerator
 * Copyright © 2022 - 2023 AceiusIO
 * Licenced under MIT
 ***********************************/

print_r("\nUseage: sssg <mode> <options>\n");
print_r("        sssg build <directory>\n\n");

print_r("sssg is a super simple tool to generate static websites fromn a directory of markdown files.\n");