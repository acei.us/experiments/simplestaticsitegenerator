# SimpleStaticSiteGenerator
## Useage
1. Create a source directory. It must contain a `config.json`. A template is attached below. Place any markdown files you want to be pages into the folder.
```json
{
    "title": "Test Site",
    "primary-color": "#000",
    "secondary-color": "#fff",
    "primary-text": "#fff",
    "secondary-text": "#000"
}
```
2. Point the PHAR archive at the directory
3. Open the dist directory to retrive your html and css